{
    "id": "f0aea35e-12b4-4683-aa7b-42434b74a868",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSquare",
    "eventList": [
        {
            "id": "f4829f2b-fb02-42a8-8467-6212c0481f1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0aea35e-12b4-4683-aa7b-42434b74a868"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fbbafaca-0524-4db8-b568-df621ea904fb",
    "visible": true
}