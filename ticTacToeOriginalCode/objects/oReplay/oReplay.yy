{
    "id": "82016d68-8706-4008-ace3-fb04287fcd79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oReplay",
    "eventList": [
        {
            "id": "5f6c5281-2029-4465-8b70-0135eca38cee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82016d68-8706-4008-ace3-fb04287fcd79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ce9d936-aaad-4e41-849f-a8e7dbc6f896",
    "visible": true
}