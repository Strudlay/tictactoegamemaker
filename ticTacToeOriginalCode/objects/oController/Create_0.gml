/// @description Controller Setup
playerTurn = false;
winner = false;
draw = false;
drawCount = 9;
var count = 0;
// Creates an array of all the squares
globalvar gridArray;
// Loops through numbers 0 to 8
for (var row = 0; row < 3; row++)
{
	for (var col = 0; col < 3; col++)
	{
		// Assigns current square to gridArray
		gridArray[row, col] = instance_find(oSquare, count);
		count++;
	}
}