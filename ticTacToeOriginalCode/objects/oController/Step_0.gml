/// @description Step sequence

// Updates controllers position
x = mouse_x
y = mouse_y;
// If winner is true, mouse can choose to replay game
if(winner) || (draw)
{
	oReplay.visible = true;	
	self.visible = false;
	// If player clicks game restarts
	if((mouse_check_button_pressed(mb_left)))
	{
		room_restart();	
	}
}
// If winner is false, the game plays as normal
else
{
	// Assigns the nearest oSpace object to variable
	var space = instance_nearest(x, y, oSquare);
	// Snaps the cursor to nearest oSpace object
	move_snap(space.x, space.y);	
}
// Runs game script
scrGame();
// Restart option for debugging
if (keyboard_check_pressed(vk_space))
{
	room_restart();	
}