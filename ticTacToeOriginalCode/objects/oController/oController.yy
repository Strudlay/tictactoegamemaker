{
    "id": "26c57a06-210a-425c-9fa3-2a7c14a2b520",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oController",
    "eventList": [
        {
            "id": "74ef1ebc-98d1-4d66-8249-50e6bb0f10ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26c57a06-210a-425c-9fa3-2a7c14a2b520"
        },
        {
            "id": "09cdffd9-0d7c-4bbc-a14a-4d4060248286",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "26c57a06-210a-425c-9fa3-2a7c14a2b520"
        },
        {
            "id": "a8228c9e-5402-43c6-b938-fb37181095d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "26c57a06-210a-425c-9fa3-2a7c14a2b520"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d8eb97e-cfa5-439c-9b3e-d296d5be2543",
    "visible": true
}