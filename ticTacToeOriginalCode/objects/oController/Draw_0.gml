/// @description Insert description here
draw_self();
// Draws numbers on squares to represent instance numbers(testing purpose)
for (var i = 0; i < instance_number(oSquare); i++)
{
	squareArray[i] = instance_find(oSquare, i);
	draw_text(squareArray[i].x - 60, squareArray[i].y - 60, "#" + string(i));
	draw_set_color(c_black);
}