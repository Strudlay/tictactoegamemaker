/// @desc Assigns values to each square with a piece, then scans the grid for a winner

/// Scans grid to check for piece collisions, then assigns values based on piece -------------- ///
for(var row = 0; row < 3; row++)
{
	for(var col = 0; col < 3; col++)
	{
		// Uses grid coordinates
		with(gridArray[row, col])
		{
			// If an O is present on current square
			if(place_meeting(x, y, oO))
			{
				// Assigns a value of 1 to said square
				var inst = instance_nearest(x, y, oSquare);
				inst.value = 1;
			}
			// If an X is present on current square
			if(place_meeting(x, y, oX))
			{
				// Assigns a value of 2 to said square
				var inst = instance_nearest(x, y, oSquare);
				inst.value = 2;
			}
		}
	}
}
/// Scans grid for draw ----------------------------------------------------------------------- ///
// Sets drawCount to 9
drawCount = 9;
// Scans grid for draw by looping through each square
for(var row = 0; row < 3; row++)
{
	for(var col = 0; col < 3; col++)
	{
		// Checks to see if current square is not free
		with(gridArray[row, col])
		{
			if(!place_free(x, y))
			{
				// If it is not free, deduct 1 from drawCount
				oController.drawCount--;	
			}
		}
	}
}
// If no draw, checks for winner
if(draw == false)
{
	/// Scans grid horizontally --------------------------------------------------------------- ///
	for(var row = 0; row < 3; row++)
	{
		for(var col = 0; col < 1; col++)
		{
			// If winner is X
			if(gridArray[row, col].value == 2) && (gridArray[row, col + 1].value == 2) && (gridArray[row, col + 2].value == 2)
			{
				// Creates winner banner and activates winner boolean
				instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oWinner);
				winner = true;
			}
			// If winner is O
			else if(gridArray[row, col].value == 1) && (gridArray[row, col + 1].value == 1) && (gridArray[row, col + 2].value == 1)
			{
				// Creates loser banner and activates winner boolean
				instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oLoser);
				winner = true;
			}
		}
	}
	/// Scans grid horizontally --------------------------------------------------------------- ///
	for(var row = 0; row < 1; row++)
	{
		for(var col = 0; col < 3; col++)
		{
			// If winner is X
			if(gridArray[row, col].value == 2) && (gridArray[row + 1, col].value == 2) && (gridArray[row + 2, col].value == 2)
			{
				// Creates winner banner and activates winner boolean
				instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oWinner);
				winner = true;
			}
			// If winner is O
			else if(gridArray[row, col].value == 1) && (gridArray[row + 1, col].value == 1) && (gridArray[row + 2, col].value == 1)
			{
				// Creates loser banner and activates winner boolean
				instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oLoser);
				winner = true;
			}
		}
	}
	/// Scans grid diagonally ----------------------------------------------------------------- ///

	// If winner is X on 0, 4, 8
	if(gridArray[0, 0].value == 2) && (gridArray[1, 1].value == 2) && (gridArray[2, 2].value == 2)
	{
		// Creates winner banner and activates winner boolean
		instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oWinner);
		winner = true;
	}
	// If winner is O on 0, 4, 8
	else if(gridArray[0, 0].value == 1) && (gridArray[1, 1].value == 1) && (gridArray[2, 2].value == 1)
	{
		// Creates loser banner and activates winner boolean
		instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oLoser);
		winner = true;
	}
	// If winner is X on 2, 4, 6
	if(gridArray[0, 2].value == 2) && (gridArray[1, 1].value == 2) && (gridArray[2, 0].value == 2)
	{
		// Creates winner banner and activates winner boolean
		instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oWinner);
		winner = true;
	}
	// If winner is O on 2, 4, 6
	else if(gridArray[0, 2].value == 1) && (gridArray[1, 1].value == 1) && (gridArray[2, 0].value == 1)
	{
		// Creates loser banner and activates winner boolean
		instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oLoser);
		winner = true;
	}
}
// If drawCount equals 0, game is draw
if(drawCount == 0) && (winner == false)
{
	// Creates draw banner and activates winner boolean
	instance_create_layer(gridArray[1, 1].x, gridArray[1, 1].y, "Message", oDraw);
	draw = true;
}
/*
// Horizontal
0, 1, 2
3, 4, 5
6, 7, 8

// Vertical check
0, 3, 6
1, 4, 7 
2, 5, 8

// Diagonal check
0   2
  4 
6   8