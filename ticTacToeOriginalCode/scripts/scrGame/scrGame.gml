/// @desc Sets up script for controls, ai player, etc.

// Sets key for user
key_left = mouse_check_button_pressed(mb_left);

// Checks to see if there is a free square
if((key_left) && (playerTurn == true) && (place_meeting(x, y, oSquare)) && (!place_meeting(x, y, oX)) && (!place_meeting(x, y, oO)) && (winner == false)) 
{
	// Creates X piece and ends player turn
	instance_create_layer(x, y, "Pieces", oX);
	scrScanner();
	playerTurn = false;
}
// Checks to see if player's turn is over
if(playerTurn == false) && (winner == false)
{
	// Continuously loops until there is a free square
	do
	{
		// Assigns random number 1-9 to pick square from array
		var row = random(3);
		var col = random(3);
		var test = gridArray[row, col];
	}
	until(place_free(test.x, test.y)); // Ends loop once it finds a free square
	// Creates piece at said square
	instance_create_layer(test.x, test.y, "Pieces", oO);
	scrScanner();
	playerTurn = true;
}
