{
    "id": "fbbafaca-0524-4db8-b568-df621ea904fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4df015bd-091f-40c8-928d-c2aa89a2f2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbbafaca-0524-4db8-b568-df621ea904fb",
            "compositeImage": {
                "id": "fa75f360-7f31-4280-8f70-c39f6094ba72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df015bd-091f-40c8-928d-c2aa89a2f2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c13835-9792-41fb-ba42-74efb7e5872b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df015bd-091f-40c8-928d-c2aa89a2f2f2",
                    "LayerId": "a3174eda-54df-4f38-97a7-8340117e0301"
                }
            ]
        },
        {
            "id": "ddfcb0c0-aa09-4de1-8162-6503319813a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbbafaca-0524-4db8-b568-df621ea904fb",
            "compositeImage": {
                "id": "45b94e21-8dd9-4ffa-ba93-732edcc64dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddfcb0c0-aa09-4de1-8162-6503319813a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43839eb-06a1-4670-b764-9f030ead3ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddfcb0c0-aa09-4de1-8162-6503319813a8",
                    "LayerId": "a3174eda-54df-4f38-97a7-8340117e0301"
                }
            ]
        },
        {
            "id": "48d05a49-309d-4486-91bf-e55320eec7ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbbafaca-0524-4db8-b568-df621ea904fb",
            "compositeImage": {
                "id": "204223ae-01fa-486c-826b-556b13c27b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d05a49-309d-4486-91bf-e55320eec7ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd702b3e-b100-4502-b06e-56d38622f490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d05a49-309d-4486-91bf-e55320eec7ee",
                    "LayerId": "a3174eda-54df-4f38-97a7-8340117e0301"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 128,
    "layers": [
        {
            "id": "a3174eda-54df-4f38-97a7-8340117e0301",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbbafaca-0524-4db8-b568-df621ea904fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}