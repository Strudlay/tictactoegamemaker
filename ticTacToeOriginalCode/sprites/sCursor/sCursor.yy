{
    "id": "3d8eb97e-cfa5-439c-9b3e-d296d5be2543",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2ff0544-712a-4779-b7ab-6d8f1f7252ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d8eb97e-cfa5-439c-9b3e-d296d5be2543",
            "compositeImage": {
                "id": "946257cf-8f59-4481-847f-e0d638a7e788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2ff0544-712a-4779-b7ab-6d8f1f7252ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bbe0912-d077-4c21-a03e-ee1bc03e2b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2ff0544-712a-4779-b7ab-6d8f1f7252ee",
                    "LayerId": "d59e5e10-40b3-4f6d-b8cc-16ecc30a1bc1"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 126,
    "layers": [
        {
            "id": "d59e5e10-40b3-4f6d-b8cc-16ecc30a1bc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d8eb97e-cfa5-439c-9b3e-d296d5be2543",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 63,
    "yorig": 63
}