{
    "id": "feb83acd-917f-4c35-b434-9ead96001ffe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDraw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 53,
    "bbox_right": 197,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ac53ef2-8af7-4813-9b2c-947b535c55c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "feb83acd-917f-4c35-b434-9ead96001ffe",
            "compositeImage": {
                "id": "6279778b-0efe-4a9f-a8e6-fa4d694de522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac53ef2-8af7-4813-9b2c-947b535c55c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fea9c26-958b-436f-a2a9-566a6432f636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac53ef2-8af7-4813-9b2c-947b535c55c6",
                    "LayerId": "ccfc3db1-6daf-4647-b8d9-851883c5feee"
                }
            ]
        },
        {
            "id": "8f979221-eddc-4b15-82bd-5be5b4865b7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "feb83acd-917f-4c35-b434-9ead96001ffe",
            "compositeImage": {
                "id": "e48d1881-44e3-4782-94c1-f350cb08e409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f979221-eddc-4b15-82bd-5be5b4865b7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab71a3de-ddc5-4d5d-990c-7ddc70e7cc32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f979221-eddc-4b15-82bd-5be5b4865b7b",
                    "LayerId": "ccfc3db1-6daf-4647-b8d9-851883c5feee"
                }
            ]
        },
        {
            "id": "696975e9-fb18-44a5-8171-999d4cd6039f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "feb83acd-917f-4c35-b434-9ead96001ffe",
            "compositeImage": {
                "id": "32528a66-3417-4248-90f7-ec0d8039b91c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "696975e9-fb18-44a5-8171-999d4cd6039f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bad4c57-a59f-4a13-8f91-ab08a708544b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "696975e9-fb18-44a5-8171-999d4cd6039f",
                    "LayerId": "ccfc3db1-6daf-4647-b8d9-851883c5feee"
                }
            ]
        },
        {
            "id": "55c5b18f-e7ef-4dc6-956e-4b06cd03a67a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "feb83acd-917f-4c35-b434-9ead96001ffe",
            "compositeImage": {
                "id": "d54962f1-ffec-4d8e-b1fe-b5e49a21ab75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c5b18f-e7ef-4dc6-956e-4b06cd03a67a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02156718-d525-47e6-9300-57c191bdd296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c5b18f-e7ef-4dc6-956e-4b06cd03a67a",
                    "LayerId": "ccfc3db1-6daf-4647-b8d9-851883c5feee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ccfc3db1-6daf-4647-b8d9-851883c5feee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "feb83acd-917f-4c35-b434-9ead96001ffe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}