{
    "id": "d4ba81a3-dde0-4348-88ff-811426fcb0c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOPiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "515d7a88-5747-4a90-995d-b41e2a5094bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ba81a3-dde0-4348-88ff-811426fcb0c0",
            "compositeImage": {
                "id": "21bd0c07-3c93-4a1e-8891-fb0ab04ed4fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515d7a88-5747-4a90-995d-b41e2a5094bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a16e3ca-3c8c-4a07-8e55-7cf51a89633c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515d7a88-5747-4a90-995d-b41e2a5094bb",
                    "LayerId": "43294423-1944-4002-a8b3-70682904a885"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 64,
    "layers": [
        {
            "id": "43294423-1944-4002-a8b3-70682904a885",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4ba81a3-dde0-4348-88ff-811426fcb0c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}