{
    "id": "37422a6b-60e5-408d-bbc8-3aa8f6897f1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWinner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 25,
    "bbox_right": 229,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3e3dbe0-fa1b-4497-af12-10dbe051d4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37422a6b-60e5-408d-bbc8-3aa8f6897f1e",
            "compositeImage": {
                "id": "cf6f4766-8c4d-4500-b4b4-7a1c477d1356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e3dbe0-fa1b-4497-af12-10dbe051d4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde00fb5-ce65-43b7-86b2-95f939c562c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e3dbe0-fa1b-4497-af12-10dbe051d4c7",
                    "LayerId": "f19b4cce-2083-4a33-9ca5-99b49589af72"
                }
            ]
        },
        {
            "id": "46c784d4-59d4-49cb-97d6-f1599f9b6524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37422a6b-60e5-408d-bbc8-3aa8f6897f1e",
            "compositeImage": {
                "id": "fb9db070-b6ac-4652-a4a1-fe46df8dfcb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c784d4-59d4-49cb-97d6-f1599f9b6524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a238cc2a-46b2-4284-8028-5ccb0543aad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c784d4-59d4-49cb-97d6-f1599f9b6524",
                    "LayerId": "f19b4cce-2083-4a33-9ca5-99b49589af72"
                }
            ]
        },
        {
            "id": "0882c7be-9688-4190-a5ef-0ab7d1f0773c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37422a6b-60e5-408d-bbc8-3aa8f6897f1e",
            "compositeImage": {
                "id": "fcf50678-5a52-4920-b5ba-dd34097bd9bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0882c7be-9688-4190-a5ef-0ab7d1f0773c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "839137ce-ecd9-4539-a38a-db07fb8c854a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0882c7be-9688-4190-a5ef-0ab7d1f0773c",
                    "LayerId": "f19b4cce-2083-4a33-9ca5-99b49589af72"
                }
            ]
        },
        {
            "id": "de93bbd6-6f7d-4c52-932c-d2637de671da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37422a6b-60e5-408d-bbc8-3aa8f6897f1e",
            "compositeImage": {
                "id": "97410273-2859-41bf-99b4-431d3c4e43ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de93bbd6-6f7d-4c52-932c-d2637de671da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0297a1-bee0-428b-b858-a691e2024f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de93bbd6-6f7d-4c52-932c-d2637de671da",
                    "LayerId": "f19b4cce-2083-4a33-9ca5-99b49589af72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f19b4cce-2083-4a33-9ca5-99b49589af72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37422a6b-60e5-408d-bbc8-3aa8f6897f1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}