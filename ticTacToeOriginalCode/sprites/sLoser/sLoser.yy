{
    "id": "693966b9-6ff9-419b-9f76-30d968f8371e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLoser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 54,
    "bbox_right": 211,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a0deddd-b1ae-4219-bb84-a490b80be6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693966b9-6ff9-419b-9f76-30d968f8371e",
            "compositeImage": {
                "id": "018bd5c7-9804-49f6-b72d-f25e3c714046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0deddd-b1ae-4219-bb84-a490b80be6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ffd0e52-67f0-41ab-b4f6-365172980966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0deddd-b1ae-4219-bb84-a490b80be6ac",
                    "LayerId": "9ac521a7-0981-42d4-8bce-0fe0540d9761"
                }
            ]
        },
        {
            "id": "7c1d6ac9-8a77-449f-8b30-d8ae34c6aa6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693966b9-6ff9-419b-9f76-30d968f8371e",
            "compositeImage": {
                "id": "429a9ebb-9424-47af-9c49-c7513c71e12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c1d6ac9-8a77-449f-8b30-d8ae34c6aa6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91af46bc-b713-4e1f-9478-ff30d7b1ae65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c1d6ac9-8a77-449f-8b30-d8ae34c6aa6a",
                    "LayerId": "9ac521a7-0981-42d4-8bce-0fe0540d9761"
                }
            ]
        },
        {
            "id": "1b85bea2-8877-4292-94b4-aea8be7bcb5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693966b9-6ff9-419b-9f76-30d968f8371e",
            "compositeImage": {
                "id": "15bfe274-faaa-42f2-b097-59faccc05db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b85bea2-8877-4292-94b4-aea8be7bcb5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65048a93-5adb-4179-9aa4-3f9ba458b4ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b85bea2-8877-4292-94b4-aea8be7bcb5a",
                    "LayerId": "9ac521a7-0981-42d4-8bce-0fe0540d9761"
                }
            ]
        },
        {
            "id": "880462f5-866e-4ea5-aa83-d40d871031f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693966b9-6ff9-419b-9f76-30d968f8371e",
            "compositeImage": {
                "id": "d0158cc0-cb94-4e22-8b27-713a6001e3db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880462f5-866e-4ea5-aa83-d40d871031f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d367f8fa-9416-4d2d-9fc9-d9e641371948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880462f5-866e-4ea5-aa83-d40d871031f2",
                    "LayerId": "9ac521a7-0981-42d4-8bce-0fe0540d9761"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9ac521a7-0981-42d4-8bce-0fe0540d9761",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "693966b9-6ff9-419b-9f76-30d968f8371e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 64
}