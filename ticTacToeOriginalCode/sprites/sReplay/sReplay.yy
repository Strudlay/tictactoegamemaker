{
    "id": "6ce9d936-aaad-4e41-849f-a8e7dbc6f896",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sReplay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa24b4e4-6f86-4b28-9cc2-1dcf409077e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ce9d936-aaad-4e41-849f-a8e7dbc6f896",
            "compositeImage": {
                "id": "4c53bde1-319b-42fd-8baf-718192bd8747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa24b4e4-6f86-4b28-9cc2-1dcf409077e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6a04a08-0b26-4971-82f8-2b22eb221bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa24b4e4-6f86-4b28-9cc2-1dcf409077e6",
                    "LayerId": "a09626f1-99a4-4c33-87ba-c5b5fdc65195"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 128,
    "layers": [
        {
            "id": "a09626f1-99a4-4c33-87ba-c5b5fdc65195",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ce9d936-aaad-4e41-849f-a8e7dbc6f896",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 64
}