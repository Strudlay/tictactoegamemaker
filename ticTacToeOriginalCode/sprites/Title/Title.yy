{
    "id": "40d4e746-83ea-4721-bdac-bbdae7c654da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 254,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a2cc1bf-dc90-422d-aef3-439665803d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d4e746-83ea-4721-bdac-bbdae7c654da",
            "compositeImage": {
                "id": "212af15c-d060-4588-884f-0e006b2db930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a2cc1bf-dc90-422d-aef3-439665803d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb48dd6-7cda-4e78-9f79-c937c05540e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a2cc1bf-dc90-422d-aef3-439665803d4e",
                    "LayerId": "15e6c926-7e28-4067-bc9d-5243eca586af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "15e6c926-7e28-4067-bc9d-5243eca586af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40d4e746-83ea-4721-bdac-bbdae7c654da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}