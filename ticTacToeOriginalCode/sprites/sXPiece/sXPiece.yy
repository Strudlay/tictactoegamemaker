{
    "id": "9c6609f4-7b1d-4208-8e1e-99021ebcdb2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sXPiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3412acd6-cb7e-4f59-8472-a467045d5f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c6609f4-7b1d-4208-8e1e-99021ebcdb2f",
            "compositeImage": {
                "id": "38935424-d6a1-4a88-ab0a-3ef70815663b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3412acd6-cb7e-4f59-8472-a467045d5f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39eea739-f80f-4612-85ba-077dcc85df13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3412acd6-cb7e-4f59-8472-a467045d5f6a",
                    "LayerId": "a82a5ad7-c96f-4e22-a329-1cf2fe80863f"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 64,
    "layers": [
        {
            "id": "a82a5ad7-c96f-4e22-a329-1cf2fe80863f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c6609f4-7b1d-4208-8e1e-99021ebcdb2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}